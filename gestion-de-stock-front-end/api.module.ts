import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient, HttpClientModule } from '@angular/common/http';


import { ArticlesService } from './api/articles.service';
import { AuthenticationService } from './api/authentication.service';
import { CategoriesService } from './api/categories.service';
import { ClientsService } from './api/clients.service';
import { CommandeClientsService } from './api/commandeClients.service';
import { CommandeFournisseurService } from './api/commandeFournisseur.service';
import { EntreprisesService } from './api/entreprises.service';
import { FournisseursService } from './api/fournisseurs.service';
import { MvtStkService } from './api/mvtStk.service';
import { PhotosService } from './api/photos.service';
import { RolesService } from './api/roles.service';
import { UtilisateursService } from './api/utilisateurs.service';
import { VentesService } from './api/ventes.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    ArticlesService,
    AuthenticationService,
    CategoriesService,
    ClientsService,
    CommandeClientsService,
    CommandeFournisseurService,
    EntreprisesService,
    FournisseursService,
    MvtStkService,
    PhotosService,
    RolesService,
    UtilisateursService,
    VentesService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration) {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
