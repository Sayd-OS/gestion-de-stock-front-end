import { ChangerMotDePasseComponent } from './pages/profil/changer-mot-de-passe/changer-mot-de-passe.component';
import { PageUtilisateurComponent } from './pages/utilisateur/page-utilisateur/page-utilisateur.component';
import { NouvelleCategorieComponent } from './composants/nouvelle-categorie/nouvelle-categorie.component';
import { PageCategoriesComponent } from './pages/categories/page-categories/page-categories.component';
import { NouvelleCmdCltFrsComponent } from './composants/nouvelle-cmd-clt-frs/nouvelle-cmd-clt-frs.component';
import { PageCmdCltFrsComponent } from './pages/page-cmd-clt-frs/page-cmd-clt-frs.component';
import { NouveauCltFrsComponent } from './composants/nouveau-clt-frs/nouveau-clt-frs.component';
import { PageFournisseurComponent } from './pages/fournisseur/page-fournisseur/page-fournisseur.component';
import { PageClientComponent } from './pages/client/page-client/page-client.component';
import { PageArticleComponent } from './pages/articles/page-article/page-article.component';
import { PageStatistiquesComponent } from './pages/page-statistiques/page-statistiques.component';
import { PageDashboardComponent } from './pages/page-dashboard/page-dashboard.component';
import { PageInscriptionComponent } from './pages/page-inscription/page-inscription.component';
import { PageLoginComponent } from './pages/page-login/page-login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNouvelArticleComponent } from './pages/articles/page-nouvel-article/page-nouvel-article.component';
import { PageMvtstkComponent } from './pages/mvtstk/page-mvtstk/page-mvtstk.component';
import { NouveauUtilisateurComponent } from './pages/utilisateur/nouveau-utilisateur/nouveau-utilisateur.component';
import { PageProfilComponent } from './pages/profil/page-profil/page-profil.component';
import {ApplicationGuardService} from './services/guard/application-guard.service';

let routes: Routes;
routes = [
  {
    path: 'login',
    component: PageLoginComponent,

  },
  {
    path: 'inscrire',
    component: PageInscriptionComponent,
  },
  {
    path: '',
    component: PageDashboardComponent,
    children: [
      {
        path: 'statistiques',
        component: PageStatistiquesComponent,
        canActivate: [ApplicationGuardService]
      },
      {
        path: 'articles',
        component: PageArticleComponent,
        canActivate: [ApplicationGuardService]
      },
      {
        path: 'nouvelArticle',
        component: PageNouvelArticleComponent,
        canActivate: [ApplicationGuardService]
      },
      {
        path: 'mvtstk',
        component: PageMvtstkComponent,
        canActivate: [ApplicationGuardService]
      },
      {
        path: 'clients',
        component: PageClientComponent,
        canActivate: [ApplicationGuardService]
      },
      {
        path: 'nouveauClient',
        component: NouveauCltFrsComponent,
        canActivate: [ApplicationGuardService],
        data: {
          origin: 'client',
        },
      },
      {
        path: 'commandeCient',
        component: PageCmdCltFrsComponent,
        canActivate: [ApplicationGuardService],
        data: {
          origin: 'client',
        },
      },
      {
        path: 'nouvelleCommandeClt',
        component: NouvelleCmdCltFrsComponent,
        canActivate: [ApplicationGuardService],
        data: {
          origin: 'client',
        },
      },
      {
        path: 'fournisseurs',
        component: PageFournisseurComponent,
        canActivate: [ApplicationGuardService],
      },
      {
        path: 'nouveauFournisseur',
        component: NouveauCltFrsComponent,
        canActivate: [ApplicationGuardService],
        data: {
          origin: 'fournisseur',
        },
      },
      {
        path: 'commandeFournisseur',
        component: PageCmdCltFrsComponent,
        canActivate: [ApplicationGuardService],
        data: {
          origin: 'fournisseur',
        },
      },
      {
        path: 'nouvelleCommandefrs',
        component: NouvelleCmdCltFrsComponent,
        canActivate: [ApplicationGuardService],
        data: {
          origin: 'fournisseur',
        },
      },
      {
        path: 'categorie',
        component: PageCategoriesComponent,
        canActivate: [ApplicationGuardService],
      },
      {
        path: 'nouvelleCategorie',
        component: NouvelleCategorieComponent,
        canActivate: [ApplicationGuardService],
      },
      {
        path: 'utilisateurs',
        component: PageUtilisateurComponent,
        canActivate: [ApplicationGuardService],
      },
      {
        path: 'nouveauUtilisateur',
        component: NouveauUtilisateurComponent,
        canActivate: [ApplicationGuardService],
      },
      {
        path: 'profil',
        component: PageProfilComponent,
      },
      {
        path: 'changerMotDePasse',
        component: ChangerMotDePasseComponent,
        canActivate: [ApplicationGuardService],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
