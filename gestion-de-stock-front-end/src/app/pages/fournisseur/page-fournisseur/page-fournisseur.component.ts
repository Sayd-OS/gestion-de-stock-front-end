import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-fournisseur',
  templateUrl: './page-fournisseur.component.html',
  styleUrls: ['./page-fournisseur.component.css']
})
export class PageFournisseurComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  nouveauFournisseur():void{
    this.router.navigate(['nouveauFournisseur']);
  }

}
