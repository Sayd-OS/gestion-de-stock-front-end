import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-categories',
  templateUrl: './page-categories.component.html',
  styleUrls: ['./page-categories.component.css'],
})
export class PageCategoriesComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  nouvelleCategorie(): void {
    this.router.navigate(['nouvelleCategorie']);
  }
}
