import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-client',
  templateUrl: './page-client.component.html',
  styleUrls: ['./page-client.component.css']
})
export class PageClientComponent implements OnInit {

  constructor(private router :Router) { }

  ngOnInit() {
  }

  nouveauClient():void{
    this.router.navigate(['nouveauClient'])
  }

}
