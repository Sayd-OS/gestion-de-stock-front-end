import { Router } from '@angular/router';
import { AuthentificationRequest } from './../../../../model/authentificationRequest';
import { UserService } from './../../services/user/user.service';
import { AdresseDto } from './../../../../model/adresseDto';
import { Component, OnInit } from '@angular/core';

import { EntrepriseService } from '../../services/entreprise/entreprise.service';
import { EntrepriseDto } from 'model/entrepriseDto';

@Component({
  selector: 'app-page-inscription',
  templateUrl: './page-inscription.component.html',
  styleUrls: ['./page-inscription.component.css'],
})
export class PageInscriptionComponent implements OnInit {
  entrepriseDto: EntrepriseDto = {};
  adresseDto: AdresseDto = {};
  errorsMesg = [];

  constructor(
    private entrepriseService: EntrepriseService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  inscrire(): void {
    this.entrepriseDto.adresse = this.adresseDto;
    this.entrepriseService.sinscrire(this.entrepriseDto).subscribe(
      () => {
        this.connectEntreprise();
      },
      (error) => {
        console.log('error:', error);
        this.errorsMesg = error.error.errors;
      }
    );
  }

  connectEntreprise(): void {
    const authenticationRequest: AuthentificationRequest = {
      login: this.entrepriseDto.email,
      password: 'som3R@nd0mP@$$w0rd',
    };
    this.userService.login(authenticationRequest).subscribe((response) => {
      this.userService.setAccessToken(response);
      this.getUserByEmail(authenticationRequest.login);
      localStorage.setItem('origin', 'inscription');
      this.router.navigate(['changerMotDePasse']);
    });
  }

  getUserByEmail(email?: string): void {
    this.userService.getUserByEmail(email).subscribe((user) => {
      this.userService.setConnectedUser(user);
    });
  }
}
