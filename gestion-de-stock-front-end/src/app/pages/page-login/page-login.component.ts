import { UserService } from './../../services/user/user.service';
import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { AuthentificationRequest } from 'model/authentificationRequest';

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.css'],
})
export class PageLoginComponent implements OnInit {
  authenticationRequest: AuthentificationRequest = {};
  errorMessage = '';
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  // tslint:disable-next-line:typedef
  login() {
    this.userService.login(this.authenticationRequest).subscribe(
      (data) => {
        this.userService.setAccessToken(data);
        this.getUserByEmail();
        this.router.navigate(['']);
      },
      (error) => {
        this.errorMessage = 'Login et / ou mot de passe incorrecte';
      }
    );
  }

  getUserByEmail(): void {
    this.userService
      .getUserByEmail(this.authenticationRequest.login)
      .subscribe((user) => {
        this.userService.setConnectedUser(user);
      });
  }
}
