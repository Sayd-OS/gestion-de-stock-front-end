import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNouvelArticleComponent } from './page-nouvel-article.component';

describe('PageNouvelArticleComponent', () => {
  let component: PageNouvelArticleComponent;
  let fixture: ComponentFixture<PageNouvelArticleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageNouvelArticleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNouvelArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
