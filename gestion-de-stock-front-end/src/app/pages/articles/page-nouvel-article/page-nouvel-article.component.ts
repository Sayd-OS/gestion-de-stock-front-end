import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-nouvel-article',
  templateUrl: './page-nouvel-article.component.html',
  styleUrls: ['./page-nouvel-article.component.css']
})
export class PageNouvelArticleComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  cancel():void{
    this.router.navigate(['articles']);
  }

}
