import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-article',
  templateUrl: './page-article.component.html',
  styleUrls: ['./page-article.component.css']
})
export class PageArticleComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  nouveauArticle():void {
    this.router.navigate(['nouvelArticle'])
  }

}
