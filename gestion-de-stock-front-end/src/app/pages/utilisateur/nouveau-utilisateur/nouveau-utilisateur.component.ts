import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nouveau-utilisateur',
  templateUrl: './nouveau-utilisateur.component.html',
  styleUrls: ['./nouveau-utilisateur.component.css']
})
export class NouveauUtilisateurComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  cancel():void{
    this.router.navigate(['utilisateurs']);
  }

}
