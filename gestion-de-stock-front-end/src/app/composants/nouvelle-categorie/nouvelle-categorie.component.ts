import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nouvelle-categorie',
  templateUrl: './nouvelle-categorie.component.html',
  styleUrls: ['./nouvelle-categorie.component.css']
})
export class NouvelleCategorieComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  cancel(): void {
    this.router.navigate(['categorie']);
  }

}
