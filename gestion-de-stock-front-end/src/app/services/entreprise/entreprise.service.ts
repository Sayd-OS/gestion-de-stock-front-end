import { Injectable } from '@angular/core';
import { EntreprisesService } from 'api/entreprises.service';
import { EntrepriseDto } from 'model/entrepriseDto';

import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {

  constructor(private entreprisesService: EntreprisesService ) { }

  sinscrire(entreprise: EntrepriseDto): Observable<EntrepriseDto> {
    return  this.entreprisesService.saveUsingPOST5(entreprise);
  }
}
