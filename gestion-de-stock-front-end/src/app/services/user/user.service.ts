import {Router} from '@angular/router';
import {Injectable} from '@angular/core';

import {Observable, of} from 'rxjs';
import {AuthenticationService} from '../../../../api/authentication.service';
import {AuthentificationRequest} from '../../../../model/authentificationRequest';
import {AuthentificationResponse} from '../../../../model/authentificationResponse';
import {UtilisateursService} from '../../../../api/utilisateurs.service';
import {UtilisateurDto} from '../../../../model/utilisateurDto';
import {ChangerMotDePasseComponent} from '../../pages/profil/changer-mot-de-passe/changer-mot-de-passe.component';
import {ChangerMotDePasseUtilisateurDto} from '../../../../model/changerMotDePasseUtilisateurDto';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private authenticationService: AuthenticationService,
    private utilisateurService: UtilisateursService,
    private router: Router
  ) {
  }

  login(authenticationRequest: AuthentificationRequest ): Observable<AuthentificationResponse> {
    return this.authenticationService.authenticateUsingPOST( authenticationRequest);
  }

  getUserByEmail(email?: string): Observable<UtilisateurDto> {
    if (email !== undefined) {
      return this.utilisateurService.findByEmailUsingGET(email);
    }
    return of();
  }

  setAccessToken(authenticationResponse: AuthentificationResponse): void {
    localStorage.setItem('accessToken', JSON.stringify(authenticationResponse));
  }

  setConnectedUser(utilisateur: UtilisateurDto): void {
    localStorage.setItem('connectedUser', JSON.stringify(utilisateur));
  }

  getConnectedUser(): UtilisateurDto {
    if (localStorage.getItem('connectedUser')) {
      return JSON.parse(localStorage.getItem('connectedUser') as string);
    }
    return {};
  }

  changerMotDePasse(changerMotDePasse: ChangerMotDePasseUtilisateurDto): Observable<ChangerMotDePasseUtilisateurDto> {
    return this.utilisateurService.changeMotDePasseUsingPOST(changerMotDePasse);
  }

  // TODO
  isUserLoggedAccessTokenValid(): boolean {
    if (localStorage.getItem('accessToken')) {
      // TODO if faut verifier si le token est valid
      return true;
    }
    this.router.navigate(['login']);
    return false;
  }
}
