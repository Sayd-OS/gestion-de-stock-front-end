import { AuthentificationResponse } from './../../../../model/authentificationResponse';
import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpInterceptorService implements HttpInterceptor {
  constructor() {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let authenticationResponse: AuthentificationResponse = {};
    if (localStorage.getItem('accessToken')) {
      authenticationResponse = JSON.parse( localStorage.getItem('accessToken') as string );
      const authReq = req.clone({
        headers: new HttpHeaders({
          Authorization: 'Bearer ' + authenticationResponse.accessToken,
        }),
      });
      return next.handle(authReq);
    }
    return next.handle(req);
  }
}
