/**
 * Gestion de stock REST API
 * Documentation de l'api pour la gestion de stock
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export interface AuthentificationResponse { 
    accessToken?: string;
}
