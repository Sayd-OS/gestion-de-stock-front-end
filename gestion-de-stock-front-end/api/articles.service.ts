/**
 * Gestion de stock REST API
 * Documentation de l'api pour la gestion de stock
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { ArticleDto } from '../model/articleDto';
import { LigneCommandeClientDto } from '../model/ligneCommandeClientDto';
import { LigneCommandeFournisseurDto } from '../model/ligneCommandeFournisseurDto';
import { LigneVenteDto } from '../model/ligneVenteDto';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class ArticlesService {

    protected basePath = 'http://localhost:8081';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * Supprimer un article
     * Cette méthode permet de supprimer un article par son ID
     * @param idArticle idArticle
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteUsingDELETE(idArticle: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteUsingDELETE(idArticle: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteUsingDELETE(idArticle: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteUsingDELETE(idArticle: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idArticle === null || idArticle === undefined) {
            throw new Error('Required parameter idArticle was null or undefined when calling deleteUsingDELETE.');
        }

        let headers = this.defaultHeaders;

        // authentication (JWT) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["authorization"]) {
            headers = headers.set('authorization', this.configuration.apiKeys["authorization"]);
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/gestiondestock/v1/articles/delete/${encodeURIComponent(String(idArticle))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findAllArticleByIdCategory
     * 
     * @param idCategory idCategory
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findAllArticleByIdCategoryUsingGET(idCategory?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<ArticleDto>>;
    public findAllArticleByIdCategoryUsingGET(idCategory?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<ArticleDto>>>;
    public findAllArticleByIdCategoryUsingGET(idCategory?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<ArticleDto>>>;
    public findAllArticleByIdCategoryUsingGET(idCategory?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (idCategory !== undefined && idCategory !== null) {
            queryParameters = queryParameters.set('idCategory', <any>idCategory);
        }

        let headers = this.defaultHeaders;

        // authentication (JWT) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["authorization"]) {
            headers = headers.set('authorization', this.configuration.apiKeys["authorization"]);
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Array<ArticleDto>>(`${this.basePath}/gestiondestock/v1/articles/filter/category/filter/${encodeURIComponent(String('idArticle'))}`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Renvoi la liste des articles
     * Cette méthode permet de rechercher et renvoyer la liste des articles qui existent
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findAllUsingGET(observe?: 'body', reportProgress?: boolean): Observable<Array<ArticleDto>>;
    public findAllUsingGET(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<ArticleDto>>>;
    public findAllUsingGET(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<ArticleDto>>>;
    public findAllUsingGET(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // authentication (JWT) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["authorization"]) {
            headers = headers.set('authorization', this.configuration.apiKeys["authorization"]);
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Array<ArticleDto>>(`${this.basePath}/gestiondestock/v1/articles/all`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Rechercher un article par son CODE
     * Cette méthode permet de rechercher un article par son CODE
     * @param codeArticle codeArticle
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findByCodeUsingGET(codeArticle: string, observe?: 'body', reportProgress?: boolean): Observable<ArticleDto>;
    public findByCodeUsingGET(codeArticle: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<ArticleDto>>;
    public findByCodeUsingGET(codeArticle: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<ArticleDto>>;
    public findByCodeUsingGET(codeArticle: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (codeArticle === null || codeArticle === undefined) {
            throw new Error('Required parameter codeArticle was null or undefined when calling findByCodeUsingGET.');
        }

        let headers = this.defaultHeaders;

        // authentication (JWT) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["authorization"]) {
            headers = headers.set('authorization', this.configuration.apiKeys["authorization"]);
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<ArticleDto>(`${this.basePath}/gestiondestock/v1/articles/filter/${encodeURIComponent(String(codeArticle))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Rechercher un article par son ID
     * Cette méthode permet de rechercher un article par son ID
     * @param idArticle idArticle
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findByIdUsingGET(idArticle: number, observe?: 'body', reportProgress?: boolean): Observable<ArticleDto>;
    public findByIdUsingGET(idArticle: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<ArticleDto>>;
    public findByIdUsingGET(idArticle: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<ArticleDto>>;
    public findByIdUsingGET(idArticle: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idArticle === null || idArticle === undefined) {
            throw new Error('Required parameter idArticle was null or undefined when calling findByIdUsingGET.');
        }

        let headers = this.defaultHeaders;

        // authentication (JWT) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["authorization"]) {
            headers = headers.set('authorization', this.configuration.apiKeys["authorization"]);
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<ArticleDto>(`${this.basePath}/gestiondestock/v1/articles/${encodeURIComponent(String(idArticle))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findHistoriqueCommandeClient
     * 
     * @param idArticle idArticle
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findHistoriqueCommandeClientUsingGET(idArticle?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<LigneCommandeClientDto>>;
    public findHistoriqueCommandeClientUsingGET(idArticle?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<LigneCommandeClientDto>>>;
    public findHistoriqueCommandeClientUsingGET(idArticle?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<LigneCommandeClientDto>>>;
    public findHistoriqueCommandeClientUsingGET(idArticle?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (idArticle !== undefined && idArticle !== null) {
            queryParameters = queryParameters.set('idArticle', <any>idArticle);
        }

        let headers = this.defaultHeaders;

        // authentication (JWT) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["authorization"]) {
            headers = headers.set('authorization', this.configuration.apiKeys["authorization"]);
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Array<LigneCommandeClientDto>>(`${this.basePath}/gestiondestock/v1/articles/historique/commandeClient/${encodeURIComponent(String(idArticle))}`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findHistoriqueCommandeFournisseur
     * 
     * @param idArticle idArticle
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findHistoriqueCommandeFournisseurUsingGET(idArticle?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<LigneCommandeFournisseurDto>>;
    public findHistoriqueCommandeFournisseurUsingGET(idArticle?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<LigneCommandeFournisseurDto>>>;
    public findHistoriqueCommandeFournisseurUsingGET(idArticle?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<LigneCommandeFournisseurDto>>>;
    public findHistoriqueCommandeFournisseurUsingGET(idArticle?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (idArticle !== undefined && idArticle !== null) {
            queryParameters = queryParameters.set('idArticle', <any>idArticle);
        }

        let headers = this.defaultHeaders;

        // authentication (JWT) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["authorization"]) {
            headers = headers.set('authorization', this.configuration.apiKeys["authorization"]);
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Array<LigneCommandeFournisseurDto>>(`${this.basePath}/gestiondestock/v1/articles/historique/commandeFournisseur/${encodeURIComponent(String(idArticle))}`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findHistoriqueVentes
     * 
     * @param idArticle idArticle
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findHistoriqueVentesUsingGET(idArticle: number, observe?: 'body', reportProgress?: boolean): Observable<Array<LigneVenteDto>>;
    public findHistoriqueVentesUsingGET(idArticle: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<LigneVenteDto>>>;
    public findHistoriqueVentesUsingGET(idArticle: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<LigneVenteDto>>>;
    public findHistoriqueVentesUsingGET(idArticle: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idArticle === null || idArticle === undefined) {
            throw new Error('Required parameter idArticle was null or undefined when calling findHistoriqueVentesUsingGET.');
        }

        let headers = this.defaultHeaders;

        // authentication (JWT) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["authorization"]) {
            headers = headers.set('authorization', this.configuration.apiKeys["authorization"]);
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Array<LigneVenteDto>>(`${this.basePath}/gestiondestock/v1/articles/historique/vente/${encodeURIComponent(String(idArticle))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Enregistrer un article
     * Cette méthode permet d&#39;enregistrer ou modifier un article
     * @param articleDto articleDto
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public saveUsingPOST(articleDto: ArticleDto, observe?: 'body', reportProgress?: boolean): Observable<ArticleDto>;
    public saveUsingPOST(articleDto: ArticleDto, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<ArticleDto>>;
    public saveUsingPOST(articleDto: ArticleDto, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<ArticleDto>>;
    public saveUsingPOST(articleDto: ArticleDto, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (articleDto === null || articleDto === undefined) {
            throw new Error('Required parameter articleDto was null or undefined when calling saveUsingPOST.');
        }

        let headers = this.defaultHeaders;

        // authentication (JWT) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["authorization"]) {
            headers = headers.set('authorization', this.configuration.apiKeys["authorization"]);
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<ArticleDto>(`${this.basePath}/gestiondestock/v1/articles/create`,
            articleDto,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
