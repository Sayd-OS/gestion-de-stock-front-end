export * from './articles.service';
import { ArticlesService } from './articles.service';
export * from './authentication.service';
import { AuthenticationService } from './authentication.service';
export * from './categories.service';
import { CategoriesService } from './categories.service';
export * from './clients.service';
import { ClientsService } from './clients.service';
export * from './commandeClients.service';
import { CommandeClientsService } from './commandeClients.service';
export * from './commandeFournisseur.service';
import { CommandeFournisseurService } from './commandeFournisseur.service';
export * from './entreprises.service';
import { EntreprisesService } from './entreprises.service';
export * from './fournisseurs.service';
import { FournisseursService } from './fournisseurs.service';
export * from './mvtStk.service';
import { MvtStkService } from './mvtStk.service';
export * from './photos.service';
import { PhotosService } from './photos.service';
export * from './roles.service';
import { RolesService } from './roles.service';
export * from './utilisateurs.service';
import { UtilisateursService } from './utilisateurs.service';
export * from './ventes.service';
import { VentesService } from './ventes.service';
export const APIS = [
  ArticlesService,
  AuthenticationService,
  CategoriesService,
  ClientsService,
  CommandeClientsService,
  CommandeFournisseurService,
  EntreprisesService,
  FournisseursService,
  MvtStkService,
  PhotosService,
  RolesService,
  UtilisateursService,
  VentesService,
];
